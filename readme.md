# Baby Mirror Light
This project uses a Raspberry Pi Pico running Arduino C++ to control a baby mirror light or any IR controlled lighting setup. The code includes various lighting modes and controls for brightness, color, and more. It also supports IR remote control and touch sensors for user interaction.

## Features
Various Lighting Modes: Includes steady, wave, fade, alternate, chase, rainbow, and more.
Brightness/Color Control: Allows setting a comfortable brightness or have fun with the color!
IR Remote Control: Supports NEC and RC6 protocol remotes for controlling the light. Naturally, the remote the baby mirror came with uses a weird protocol I haven't worked out just yet. They sell those RGB remotes everywhere for $4-10 USD which work perfectly, but almost any remote will work. 
Touch Sensors: Supports touch sensors for additional user interaction. (Used to work in CircuitPython, not working here yet. Recommend using an i2c touch device for now. 

## Hardware Requirements
Raspberry Pi Pico running Arduino C++
Infrared (IR) receiver
NeoPixel LEDs
Touch sensors / i2C touch mux (optional)
Remote (optional)

## Software Requirements
Arduino IDE with support for Raspberry Pi Pico
Adafruit_NeoPixel library
IRremote library
Adafruit_TinyUSB library

## Installation
1. Install Arduino IDE and set it up for Raspberry Pi Pico as per the instructions here.
2. Install the necessary libraries (Adafruit_NeoPixel, IRremote, Adafruit_TinyUSB) using the Library Manager in Arduino IDE.
3. Clone this repository to your local machine.
4. Open babymirror.ino in Arduino IDE.
5. Connect your Raspberry Pi Pico to your computer.
6. In the Boards Manager, install Raspberry Pi Pico/RP2040 by Earle F. Pholhower, III.
7. Under Tools, set the Board and port to your device
8. Under USB Stack choose Adafruit TinyUSB
9. Upload the code to your Raspberry Pi Pico.

## Usage
1. Connect the NeoPixel LEDs, IR receiver, and touch sensors to the appropriate pins on the Raspberry Pi Pico as defined in babymirror.ino.
2. Power up the Raspberry Pi Pico.
3. Control the light using the connected IR remote or touch sensors.
4. Enjoy your Baby Mirror Light!