#include <Adafruit_NeoPixel.h>
#include <IRremote.h>
#include "Adafruit_TinyUSB.h"

#define PIXEL_PIN 0
#define PIXEL_COUNT 9
#define PIXEL_TYPE NEO_GRB  // + NEO_KHZ800

Adafruit_NeoPixel pixels(PIXEL_COUNT, PIXEL_PIN, PIXEL_TYPE);

#define TOUCH_PIN_0 1
#define TOUCH_PIN_1 3
#define TOUCH_PIN_2 4
#define TOUCH_PIN_3 5

#define IR_PIN 2
IRrecv irrecv(IR_PIN);
decode_results results;

#define PHOTOCELL_PIN 27
#define TOUCH_THRESHOLD 27
// int photocellValue = 0;


unsigned long lastInputTime = 0;
unsigned long turnOffDelay = 20000;  // Turn off after 20 seconds of no input
bool buttonPressed = false;          // Tracks whether any button has been pressed


bool lightState = false;
float brightnessStore = 0.1;
uint32_t storedColor = 0xFFFFFF;
float brightnessTarget = 0.1;
int colorIndex = 0;

//color references: R 2, G 10, B 6, W 0
uint32_t colors[] = { 0xFFFFFF, 0x419FFF, 0xFF0000, 0xFF0080, 0xFF00FF, 0x8000FF, 0x0000FF, 0x0080FF, 0x00FFFF, 0x00FF80, 0x00FF00, 0x80FF00, 0xFFFF00, 0xFF8000 };
uint32_t colors2[] = { 0xFF4000, 0x15FF44, 0x0362fc, 0xFF1500, 0x15DDfF, 0xdb03fc, 0xFF7011, 0x008cff, 0xee00BB, 0xff8800, 0x00BBff, 0xff0088 };
uint32_t targetColor = colors[0];


bool touching_0 = false;
bool touchHeld_0 = false;
unsigned long lastTouchTime_0 = 0;

bool touching_1 = false;
bool touchHeld_1 = false;
unsigned long lastTouchTime_1 = 0;

bool touching_2 = false;
bool touchHeld_2 = false;
unsigned long lastTouchTime_2 = 0;

bool touching_3 = false;
bool touchHeld_3 = false;
unsigned long lastTouchTime_3 = 0;

int LEDMode = 0;


bool fuzzyPulseCompare(int* pulse1, int* pulse2, int len1, int len2, float fuzziness = 0.2) {
  if (len1 != len2) {
    return false;
  }
  for (int i = 0; i < len1; i++) {
    int threshold = int(pulse1[i] * fuzziness);
    if (abs(pulse1[i] - pulse2[i]) > threshold) {
      return false;
    }
  }
  return true;
}

void lightToggle(int dir = -1) {
  if (dir == 0 || (lightState && dir == -1)) {
    lightState = false;
    //brightnessStore = brightnessTarget;
    //brightnessTarget = 0;
  } else {
    lightState = true;
    int photocellValue = analogRead(PHOTOCELL_PIN);
    SerialTinyUSB.println(photocellValue);
    if (photocellValue < 100) {
      brightnessTarget = 0.2;
    } else if (photocellValue > 600) {
      brightnessTarget = 1;
    }
    // else {
    //   brightnessTarget = brightnessStore;
    // }
  }
}

void lightBrightness(float change) {
  // if (!lightState) {
  //   brightnessTarget = brightnessStore;
  //   lightState = true;
  // }
  if (!lightState) {
    lightToggle(1);
  }
  brightnessTarget += change;
  if (brightnessTarget < 0.01) {
    brightnessTarget = 0.01;
  } else if (brightnessTarget > 1) {
    brightnessTarget = 1;
  }
}

void lightColors(int dir) {
  colorIndex += dir;
  if (colorIndex >= sizeof(colors) / sizeof(colors[0])) {
    colorIndex = 0;
  } else if (colorIndex < 0) {
    colorIndex = sizeof(colors) / sizeof(colors[0]) - 1;
  }
  if (!lightState) {
    lightToggle(1);
  }
  //setLedMode(0);
}

void setSpecColor(int theBank, int theIndex) {

  SerialTinyUSB.print("setSpecColor bank ");
  SerialTinyUSB.print(theBank);
  SerialTinyUSB.print(" - from ");
  SerialTinyUSB.print(targetColor, HEX);
  if (theBank == 0) {
    targetColor = colors[theIndex];
  } else {
    targetColor = colors2[theIndex];
  }
  storedColor = targetColor;
  SerialTinyUSB.print(" to ");
  SerialTinyUSB.println(targetColor, HEX);
  //lightToggle(1);
  //setLedMode(0);
  if (!lightState) {
    lightToggle(1);
  }
}

void lightSet(uint32_t newColor) {
  targetColor = newColor;
  storedColor = targetColor;
  //brightnessTarget = brightnessStore;
  if (!lightState) {
    lightToggle(1);
  }
}

void setLedMode(int mode) {
  LEDMode += mode;
  if (LEDMode > 13) {
    LEDMode = 0;
  } else if (LEDMode < 0) {
    LEDMode = 13;
  }
  if (!lightState) {
    lightToggle(1);
  }
}



// Global variables
float bright = 0;
float brightnessIncrement = 0.000001;
float fadeFactor = 200;
float fadeFactor2 = 900;
unsigned long lastBrightnessUpdateTime = 0;
uint32_t currentColor = 0x000000;

//Brightness loop
void brightLoop() {
  //if (millis() - lastBrightnessUpdateTime >= 5) {  // Update every 5 ms
  //    lastBrightnessUpdateTime = millis();

  if (bright != brightnessTarget && lightState) {

    SerialTinyUSB.print("Brightness Update");
    if (bright > brightnessTarget + brightnessIncrement || !lightState && bright > 0) {
      bright -= ((bright - brightnessTarget) / fadeFactor) + brightnessIncrement;
    } else if (lightState && bright < brightnessTarget - brightnessIncrement) {
      bright += ((brightnessTarget - bright) / fadeFactor) + brightnessIncrement;
    } else if (lightState) {
      bright = brightnessTarget;
    } else {
      bright = 0;
    }
  } else if (!lightState) {
    if (bright > 0) {
      bright -= (bright / fadeFactor) + brightnessIncrement;
    } else {
      bright = 0;
    }
  }
  //}
}

float colorTransitionSpeed = 0.0001;
float snapRange = 0.02;
uint32_t colorQueue[5] = { 0x000000, 0x000000, 0x000000, 0x000000, 0x000000 };
unsigned long lastColorUpdateTime = 0;

#define WAVE_SPEED 0.02          // Speed of the wave
#define FADE_SPEED 0.004         // Speed of the wave
#define ALT_UPDATE_INTERVAL 150  //SPeed of alternating fx
#define WAVE_WIDTH 3             // Width of the wave
int rainbowMod = 12;
unsigned long lastUpdate = 0;    // Variable to store the last update time
unsigned long lastFXUpdate = 0;  //Variable to store FX update time for timers
bool altState = false;
int updateInterval = 5;  // Update interval in milliseconds
float fxSpeed = 1.0;     // FX speed multiplier, default is 1

void setFXSpeed(float change) {
  fxSpeed += change;
  if (fxSpeed < 0.1) {
    fxSpeed = 0.1;
  } else if (fxSpeed > 2) {
    fxSpeed = 2;
  }
}

void steadyMode() {
  pixels.fill(currentColor);
}

void waveMode(int dir = 1) {
  for (int i = 0; i < PIXEL_COUNT; i++) {
    float wavePosition = fabs((millis() * WAVE_SPEED * fxSpeed + i * dir) / WAVE_WIDTH);
    float brightness = (sin(wavePosition) * 0.5 + 0.5);  // Calculate the brightness (0-1)
    uint8_t newColor[3];
    for (int j = 0; j < 3; j++) {
      uint8_t currentComponent = (currentColor >> ((2 - j) * 8)) & 0xFF;  // Extract in RGB order
      newColor[j] = currentComponent * brightness;
    }
    uint32_t color = (newColor[0] << 16) | (newColor[1] << 8) | newColor[2];  // Pack in RGB order
    pixels.setPixelColor(i, color);
  }
}



void fadeMode() {
  float fadePosition = millis() * FADE_SPEED * fxSpeed;
  float brightness = (sin(fadePosition) * 0.5 + 0.5);  // Calculate the brightness (0-1)
  uint8_t newColor[3];
  for (int i = 0; i < 3; i++) {
    uint8_t currentComponent = (currentColor >> ((2 - i) * 8)) & 0xFF;  // Extract in RGB order
    newColor[i] = currentComponent * brightness;
  }
  uint32_t color = (newColor[0] << 16) | (newColor[1] << 8) | newColor[2];  // Pack in RGB order
  pixels.fill(color);
}


void alternateMode() {
  if (millis() - lastFXUpdate > (ALT_UPDATE_INTERVAL / fxSpeed)) {  // Check if updateInterval has passed
    lastFXUpdate = millis();                                        // Update the last update time
    for (int i = 0; i < PIXEL_COUNT; i++) {
      if (altState) {  // Even-numbered LEDs
        pixels.setPixelColor(i, currentColor);
        altState = false;
      } else {                       // Odd-numbered LEDs
        pixels.setPixelColor(i, 0);  // Turn off the LED
        altState = true;
      }
    }
  }
}

void chaseMode(int dir = 1) {
  long chasePosition = ((long)(millis() / (100 / fxSpeed)) % PIXEL_COUNT) * dir;  // Calculate the chase position
  if (chasePosition < 0) {
    chasePosition += PIXEL_COUNT;
  }
  for (int i = 0; i < PIXEL_COUNT; i++) {
    if (i % 3 == chasePosition % 3) {  // Every third LED
      pixels.setPixelColor(i, currentColor);
    } else {
      pixels.setPixelColor(i, 0);  // Turn off the LED
    }
  }
}

// Rainbow mode
void rainbowMode() {
  for (int i = 0; i < PIXEL_COUNT; i++) {
    uint8_t wheelPos = ((int)(i + millis() / (10 / fxSpeed))) % 256;  // Calculate the wheel position
    uint32_t color1 = RainbowWheel(wheelPos);
    uint8_t newColor[3];
    for (int j = 0; j < 3; j++) {
      uint8_t currentComponent = (color1 >> ((2 - j) * 8)) & 0xFF;  // Extract in RGB order
      newColor[j] = currentComponent;
    }
    uint32_t color = (newColor[0] << 16) | (newColor[1] << 8) | newColor[2];  // Pack in RGB order
    pixels.setPixelColor(i, color);
  }
}

uint32_t RainbowWheel(byte WheelPos) {
  if (WheelPos < 85) {
    return pixels.Color((255 - WheelPos * 3) * bright, 0, WheelPos * 3 * bright);
  } else if (WheelPos < 170) {
    WheelPos -= 85;
    return pixels.Color(0, WheelPos * 3 * bright, (255 - WheelPos * 3) * bright);
  } else {
    WheelPos -= 170;
    return pixels.Color(WheelPos * 3 * bright, (255 - WheelPos * 3) * bright, 0);
  }
}

void centerWaveMode(int dir = 1) {
  int center = PIXEL_COUNT / 2;
  for (int i = 0; i < PIXEL_COUNT; i++) {
    float wavePosition = fabs((millis() * WAVE_SPEED * fxSpeed + abs(center - i) * dir) / WAVE_WIDTH);
    float brightness = (sin(wavePosition) * 0.5 + 0.5);  // Calculate the brightness (0-1)
    uint8_t newColor[3];
    for (int j = 0; j < 3; j++) {
      uint8_t currentComponent = (currentColor >> ((2 - j) * 8)) & 0xFF;  // Extract in RGB order
      newColor[j] = currentComponent * brightness;
    }
    uint32_t color = (newColor[0] << 16) | (newColor[1] << 8) | newColor[2];  // Pack in RGB order
    pixels.setPixelColor(i, color);
  }
}

void rainbowWaveMode(int dir = 1) {
  for (int i = 0; i < PIXEL_COUNT; i++) {
    uint8_t wheelPos = ((int)(i * rainbowMod + millis() / (10 / fxSpeed) * dir)) % 256;  // Calculate the wheel position
    uint32_t color1 = RainbowWheel(wheelPos);
    uint8_t newColor[3];
    for (int j = 0; j < 3; j++) {
      uint8_t currentComponent = (color1 >> ((2 - j) * 8)) & 0xFF;  // Extract in RGB order
      newColor[j] = currentComponent;
    }
    uint32_t color = (newColor[0] << 16) | (newColor[1] << 8) | newColor[2];  // Pack in RGB order
    pixels.setPixelColor(i, color);
  }
}

void centerRainbowWaveMode(int dir = 1) {
  int center = PIXEL_COUNT / 2;
  for (int i = 0; i < PIXEL_COUNT; i++) {
    uint8_t wheelPos = ((int)(abs(center - i) * rainbowMod + millis() / (10 / fxSpeed) * dir)) % 256;  // Calculate the wheel position
    uint32_t color1 = RainbowWheel(wheelPos);
    uint8_t newColor[3];
    for (int j = 0; j < 3; j++) {
      uint8_t currentComponent = (color1 >> ((2 - j) * 8)) & 0xFF;  // Extract in RGB order
      newColor[j] = currentComponent;
    }
    uint32_t color = (newColor[0] << 16) | (newColor[1] << 8) | newColor[2];  // Pack in RGB order
    pixels.setPixelColor(i, color);
  }
}

void neopixelLoop() {
  if (millis() - lastUpdate > updateInterval) {  // Check if updateInterval has passed
    lastUpdate = millis();                       // Update the last update time
    uint8_t newTargetColor = 0x000000;
    uint8_t bright_target[3];
    for (int i = 0; i < 3; i++) {
      uint8_t targetComponent = (targetColor >> ((2 - i) * 8)) & 0xFF;  // Extract in RGB order
      bright_target[i] = targetComponent * bright;
    }
    newTargetColor = (bright_target[0] << 16) | (bright_target[1] << 8) | bright_target[2];  // Pack in RGB order
    //Update the color
    if (currentColor != newTargetColor) {  //bright >= 0.1 &&
      uint8_t newColor[3];
      for (int i = 0; i < 3; i++) {
        //uint8_t targetComponent = (targetColor >> ((2 - i) * 8)) & 0xFF;    // Extract in RGB order
        uint8_t currentComponent = (currentColor >> ((2 - i) * 8)) & 0xFF;  // Extract in RGB order
        if (currentComponent < bright_target[i]) {
          newColor[i] = currentComponent + 1;
        } else if (currentComponent > bright_target[i]) {
          newColor[i] = currentComponent - 1;
        } else {
          newColor[i] = bright_target[i];
        }
      }
      currentColor = (newColor[0] << 16) | (newColor[1] << 8) | newColor[2];  // Pack in RGB order

      // SerialTinyUSB.print("target: ");
      // SerialTinyUSB.print(targetColor, HEX);
      // SerialTinyUSB.print(" bright: ");
      // SerialTinyUSB.print(bright);
      // SerialTinyUSB.print(" newtarget: ");
      // SerialTinyUSB.print(newTargetColor, HEX);
      // SerialTinyUSB.print(" current: ");
      // SerialTinyUSB.println(currentColor, HEX);
    }
    //if (bright >= 0.1) {
    switch (LEDMode) {
      case 0:  // Steady mode
        steadyMode();
        break;

      case 1:  // Wave mode
        fadeMode();
        break;

      case 2:  // Wave mode
        waveMode();
        break;

      case 3:  // Fade mode
        waveMode(-1);
        break;


      case 4:  // Wave mode
        centerWaveMode();
        break;

      case 5:  // Fade mode
        centerWaveMode(-1);
        break;

      case 6:  // Alternate mode
        alternateMode();
        break;

      case 7:  // Chase mode
        chaseMode();
        break;

      case 8:  // Chase mode
        chaseMode(-1);
        break;

      case 9:  // Rainbow mode
        rainbowMode();
        break;

      case 10:  // Rainbow mode
        rainbowWaveMode();
        break;

      case 11:  // Rainbow mode
        rainbowWaveMode(-1);
        break;

      case 12:  // Rainbow mode
        centerRainbowWaveMode();
        break;

      case 13:  // Rainbow mode
        centerRainbowWaveMode(-1);
        break;
    }
    //}
    pixels.show();
  }
}


// Global variables
unsigned long lastIrPressTime = 0;
unsigned long debounceTime = 200;  // Debounce time in ms

const char* protocolToString(decode_type_t protocol) {
  switch (protocol) {
    case UNKNOWN: return "UNKNOWN";
    case PULSE_WIDTH: return "PULSE WIDTH";
    case PULSE_DISTANCE: return "PULSE DISTANCE";
    case NEC: return "NEC";
    case SONY: return "SONY";
    case RC5: return "RC5";
    case RC6: return "RC6";
    case JVC: return "JVC";
    case PANASONIC: return "PANASONIC";
    case SAMSUNG: return "SAMSUNG";
    case SAMSUNG48: return "SAMSUNG48";
    case LG: return "LG";
    case LG2: return "LG2";
    case APPLE: return "APPLE";
    case MAGIQUEST: return "MAGIQUEST";
    case LEGO_PF: return "LEGO_PF";
    case DENON: return "DENON";
    case SHARP: return "SHARP";
    case NEC2: return "NEC2";
    case ONKYO: return "ONKYO";
    case KASEIKYO: return "KASEIKYO";
    case KASEIKYO_DENON: return "KASEIKYO_DENON";
    case KASEIKYO_SHARP: return "KASEIKYO_SHARP";
    case KASEIKYO_JVC: return "KASEIKYO_JVC";
    case KASEIKYO_MITSUBISHI: return "KASEIKYO_MITSUBISHI";
    case SAMSUNG_LG: return "SAMSUNG_LG";
    case BANG_OLUFSEN: return "BANG_OLUFSEN";
    case BOSEWAVE: return "BOSEWAVE";
    case WHYNTER: return "WHYNTER";
    case FAST: return "FAST";
  }
  return "Error";
}
IRData lastResults;    // Last decoded IR results
int repeatCount = 0;   // Number of consecutive repeat flags
bool keyHeld = false;  // Whether a key is being held down

// Input loop
void inputLoop() {
  unsigned long currentTime = millis();
  if (irrecv.decode()) {
    IRData result = irrecv.decodedIRData;
    unsigned long currentTime = millis();
    if (currentTime - lastIrPressTime >= debounceTime) {
      lastIrPressTime = currentTime;

      // Handle the IR code
      // ...
      String irProto = protocolToString(result.protocol);

      SerialTinyUSB.print("Received IR code: ");
      SerialTinyUSB.print(result.address, HEX);
      SerialTinyUSB.print(":");
      SerialTinyUSB.print(result.command, HEX);
      SerialTinyUSB.print("(");
      SerialTinyUSB.print(irProto);
      SerialTinyUSB.print(") Flags: ");
      SerialTinyUSB.println(result.flags, BIN);


      // Check if the repeat flag is set
      if (result.flags == IRDATA_FLAGS_IS_REPEAT) {
        // Check if the command and address are the same as the last results
        if (result.command == lastResults.command && result.address == lastResults.address) {
          repeatCount++;
          if (repeatCount >= 3) {
            keyHeld = true;
          }
        } else {
          repeatCount = 0;
          keyHeld = false;
        }
      } else {
        repeatCount = 0;
        keyHeld = false;
      }
      // Store the results for the next iteration
      lastResults = result;

      ////////////////////////////////////////
      // NEC protocol remotes
      //////////////////////////////////////////////////////////////////////////////////////////////////////////

      if (irProto == "NEC") {
        ////////////////////////////////////////
        // LG TV remote
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (result.address == 0x4) {

          //LG TV remote - 0
          if (result.command == 0x10 && (result.flags == 0 || keyHeld)) {
            lightToggle();
            buttonPressed = true;
          }
          //LG TV remote - 1
          else if (result.command == 0x11 && (result.flags == 0 || keyHeld)) {
            buttonPressed = true;
            lightToggle(0);
          }
          //LG TV remote - 2
          else if (result.command == 0x12 && (result.flags == 0 || keyHeld)) {
            buttonPressed = true;
            lightToggle(1);
          }
          //LG TV remote - 3
          else if (result.command == 0x13 && (result.flags == 0 || keyHeld)) {
            buttonPressed = true;
            lightColors(1);
          }
          //LG TV remote - 4
          else if (result.command == 0x14 && (result.flags == 0 || keyHeld)) {
            buttonPressed = true;
            lightBrightness(-0.1);
          }
          //LG TV remote - 5
          else if (result.command == 0x15 && (result.flags == 0 || keyHeld)) {
            buttonPressed = true;
            lightBrightness(0.1);
          }
          //LG TV remote - 6
          else if (result.command == 0x16 && (result.flags == 0 || keyHeld)) {
            buttonPressed = true;
            lightColors(-1);
          }
          //LG TV remote - 7
          else if (result.command == 0x17 && (result.flags == 0 || keyHeld)) {
            buttonPressed = true;
            setLedMode(1);
          }
          //LG TV remote - 8
          else if (result.command == 0x18 && (result.flags == 0 || keyHeld)) {
            buttonPressed = true;
            setLedMode(-1);
          }
          //LG TV remote - 9
          else if (result.command == 0x19 && (result.flags == 0 || keyHeld)) {

            pixels.setBrightness(32);
            pixels.setPixelColor(0, 0x0000FF);
            pixels.setPixelColor(1, 0x00FF00);
            pixels.setPixelColor(2, 0x000000);
            pixels.setPixelColor(3, 0x333333);
            pixels.setPixelColor(4, 0xFFFFFF);
            pixels.setPixelColor(5, 0x333333);
            pixels.setPixelColor(6, 0x000000);
            pixels.setPixelColor(7, 0x00FF00);
            pixels.setPixelColor(8, 0x0000FF);
            pixels.show();
            rp2040.rebootToBootloader();
          }
          //LG TV remote - Info
          else if (result.command == 0xAA && (result.flags == 0 || keyHeld)) {
            rp2040.reboot();
          }

        }
        ////////////////////////////////////////
        // Munchkin Rocker remote
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        else if (result.address == 0x30) {

          //Munchkin remote - Power
          if (result.command == 0xC && (result.flags == 0 || keyHeld)) {
            lightToggle();
            buttonPressed = true;
          }
          //Munchkin remote - Loop
          else if (result.command == 0x17 && (result.flags == 0 || keyHeld)) {
            lightColors(0);
            buttonPressed = true;
          }
          //Munchkin remote - Timer
          else if (result.command == 0x8 && (result.flags == 0 || keyHeld)) {
           
            rp2040.reboot();
          }
          //Munchkin remote - PlayPause
          else if (result.command == 0xF && (result.flags == 0 || keyHeld)) {
            setLedMode(1);
            buttonPressed = true;
          }
          //Munchkin remote - Skip
          else if (result.command == 0xA && (result.flags == 0 || keyHeld)) {
            lightBrightness(-0.1);
            buttonPressed = true;
          }
          //Munchkin remote - Sound
          else if (result.command == 0x16 && (result.flags == 0 || keyHeld)) {
            lightBrightness(0.1);
            buttonPressed = true;
          }

        }
        ////////////////////////////////////////
        // Magic remote
        //////////////////////////////////////////////////////////////////////////////////////////////////////////

        else if (result.address == 0x0) {
          //Magic remote - Bright up
          if (result.command == 0x5 && (result.flags == 0 || keyHeld)) {
            lightBrightness(0.1);
            buttonPressed = true;
          }
          //Magic remote - Bright down
          else if (result.command == 0x4 && (result.flags == 0 || keyHeld)) {
            lightBrightness(-0.1);
            buttonPressed = true;
          }
          //Magic remote - Off
          else if (result.command == 0x6 && (result.flags == 0 || keyHeld)) {
            lightToggle(0);
            buttonPressed = true;
          }
          //Magic remote - On
          else if (result.command == 0x7 && (result.flags == 0 || keyHeld)) {
            lightToggle(1);
            buttonPressed = true;
          }
          ///////////////////////////
          //Magic remote - R
          else if (result.command == 0x9 && (result.flags == 0 || keyHeld)) {
            setSpecColor(0, 2);
            buttonPressed = true;
          }
          //Magic remote - G
          else if (result.command == 0x8 && (result.flags == 0 || keyHeld)) {
            setSpecColor(0, 10);
            buttonPressed = true;
          }
          //Magic remote - B
          else if (result.command == 0xA && (result.flags == 0 || keyHeld)) {
            setSpecColor(0, 6);
            buttonPressed = true;
          }
          //Magic remote - W
          else if (result.command == 0xB && (result.flags == 0 || keyHeld)) {
            setSpecColor(0, 0);
            buttonPressed = true;
          }

          ///////////////////////////
          //Magic remote - Color 0
          else if (result.command == 0xD && (result.flags == 0 || keyHeld)) {
            setSpecColor(1, 0);
            buttonPressed = true;
          }
          //Magic remote - Color 1
          else if (result.command == 0xC && (result.flags == 0 || keyHeld)) {
            setSpecColor(1, 1);
            buttonPressed = true;
          }
          //Magic remote - Color 2
          else if (result.command == 0xE && (result.flags == 0 || keyHeld)) {
            setSpecColor(1, 2);
            buttonPressed = true;
          }
          //Magic remote - Flash
          else if (result.command == 0xF && (result.flags == 0 || keyHeld)) {
            setLedMode(-1);
            buttonPressed = true;
          }

          ///////////////////////////
          //Magic remote - Color 3
          else if (result.command == 0x15 && (result.flags == 0 || keyHeld)) {
            setSpecColor(1, 3);
            buttonPressed = true;
          }
          //Magic remote - Color 4
          else if (result.command == 0x14 && (result.flags == 0 || keyHeld)) {
            setSpecColor(1, 4);
            buttonPressed = true;
          }
          //Magic remote - Color 5
          else if (result.command == 0x16 && (result.flags == 0 || keyHeld)) {
            setSpecColor(1, 5);
            buttonPressed = true;
          }
          //Magic remote - Strobe
          else if (result.command == 0x17 && (result.flags == 0 || keyHeld)) {
            setLedMode(1);
            buttonPressed = true;
          }

          ///////////////////////////
          //Magic remote - Color 6
          else if (result.command == 0x19 && (result.flags == 0 || keyHeld)) {
            setSpecColor(1, 6);
            buttonPressed = true;
          }
          //Magic remote - Color 7
          else if (result.command == 0x18 && (result.flags == 0 || keyHeld)) {
            setSpecColor(1, 7);
            buttonPressed = true;
          }
          //Magic remote - Color 8
          else if (result.command == 0x1A && (result.flags == 0 || keyHeld)) {
            setSpecColor(1, 8);
            buttonPressed = true;
          }
          //Magic remote - Fade
          else if (result.command == 0x1B && (result.flags == 0 || keyHeld)) {
            setFXSpeed(-0.1);
            buttonPressed = true;
          }

          ///////////////////////////
          //Magic remote - Color 9
          else if (result.command == 0x11 && (result.flags == 0 || keyHeld)) {
            setSpecColor(1, 9);
            buttonPressed = true;
          }
          //Magic remote - Color 10
          else if (result.command == 0x10 && (result.flags == 0 || keyHeld)) {
            setSpecColor(1, 10);
            buttonPressed = true;
          }
          //Magic remote - Color 11
          else if (result.command == 0x12 && (result.flags == 0 || keyHeld)) {
            setSpecColor(1, 11);
            buttonPressed = true;
          }
          //Magic remote - Smooth
          else if (result.command == 0x13 && (result.flags == 0 || keyHeld)) {
            //rp2040.reboot();
            setFXSpeed(0.1);
          }
        }
      }

      ////////////////////////////////////////
      // RC6 protocol remotes
      //////////////////////////////////////////////////////////////////////////////////////////////////////////

      else if (irProto == "RC6") {
        ////////////////////////////////////////
        // eHome remote
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (result.address == 0x4) {

          //Start
          if (result.command == 0xD && (result.flags == 100000000 || keyHeld)) {
            
            rp2040.reboot();
          }
        }
      } 
          //0: (NEC)

          // 5, 4, 6, 7
          // 9, 8, A, B
          // D, C, E, F
          // 15, 14, 16, 17
          // 19, 18, 1A, 1B
          // 11, 10, 12, 13

          //colors2

          irrecv.resume();  // Receive the next value
        }
      }
      // Read the touch sensors

      int touch_0 = analogRead(TOUCH_PIN_0);
      int touch_1 = analogRead(TOUCH_PIN_1);
      int touch_2 = analogRead(TOUCH_PIN_2);
      int touch_3 = analogRead(TOUCH_PIN_3);
      if (touch_0 + touch_1 + touch_2 + touch_3 > 0) {

        SerialTinyUSB.print("Touch: ");
        SerialTinyUSB.print(touch_0);
        SerialTinyUSB.print(" ");
        SerialTinyUSB.print(touch_1);
        SerialTinyUSB.print(" ");
        SerialTinyUSB.print(touch_2);
        SerialTinyUSB.print(" ");
        SerialTinyUSB.println(touch_3);
      }
      // Handle touch_0
      if (touch_0 > TOUCH_THRESHOLD) {
        if (!touching_0) {
          touching_0 = true;
          lastTouchTime_0 = currentTime;
        } else if (currentTime > lastTouchTime_0 + 500) {
          lightColors(1);
          buttonPressed = true;
          touchHeld_0 = true;
          lastTouchTime_0 = currentTime;
        }
      } else if (touching_0) {
        if (!touchHeld_0) {
          lightToggle();
          buttonPressed = true;
        }
        touchHeld_0 = false;
        touching_0 = false;
      }
    }


    void setup() {

      pixels.begin();
      pixels.setBrightness(255);  // Range: 0-255
      pixels.fill(0x00000000);
      pixels.show();
      // Manual begin() is required on core without built-in support for TinyUSB such as mbed rp2040
      //TinyUSB_Device_Init(0);
      SerialTinyUSB.begin(115200);



      // pinMode(TOUCH_PIN_0, INPUT);
      // pinMode(TOUCH_PIN_1, INPUT);
      pinMode(TOUCH_PIN_2, INPUT);
      pinMode(TOUCH_PIN_3, INPUT);
      pinMode(TOUCH_PIN_0, INPUT_PULLUP);    // Enable internal pull-up resistor
      pinMode(TOUCH_PIN_1, INPUT_PULLDOWN);  // Enable internal pull-up resistor

      irrecv.enableIRIn();  // Start the receiver

      pinMode(PHOTOCELL_PIN, INPUT);
      lightToggle(1);
    }

    void loop() {
      brightLoop();

      neopixelLoop();
      inputLoop();
      if (SerialTinyUSB.available()) {
        // Read the input as a string
        String input = SerialTinyUSB.readString();

        // Check if the input is "dfu"
        if (input == "dfu") {
          // Reset to bootloader

          rp2040.rebootToBootloader();
        }
      }
      // Turn off the light after 20 seconds of no input, unless a button has been pressed
      if (!buttonPressed && millis() - lastInputTime >= turnOffDelay) {
        lightToggle(0);
      }
    }


  // void loop1() {
  // }